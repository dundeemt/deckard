=======
deckard
=======
was a replicant hunter and so is this app.  Or more precisely, a duplicate file finder/reporting utility.  I work with lots of pack-rat, file-name-mangling, never delete or care about disk space graphics types and I needed a utility to give me some metrics on file duplication.  While there are lots of dupe utilities out there, most seem geared for brute force deletion and I just wanted statistics.

.. sourcecode:: bash

    ./deckard.py --seek .|./deckard.py --test|./deckard.py --report


Deckard eats it's own output through each stage.  This is helpful because you can inject other filters (i.e. grep, etc) between each stage.  You can also save deckard's work in between stages.  If you are recursing through lots of directories and files, you might not want to run --seek or --test each time you make a modification to your research.    For example:

.. sourcecode:: bash

    ./deckard.py --seek .| grep -v \.hg |./deckard.py --test|./deckard.py --report

Here I am telling it to filter out the .hg subdirectories before the testing stage.

You might see how this could be a "very good thing."

Currently deckard requires that files have the same name, size and md5 check sum to be counted as a duplicate.  While I know this is a limited subset of true duplicates, it is the low hanging fruit that I wanted more information about now.

deckard is also smart about how it applies tests.  For instance, md5sums are not calculated for each file.  Only when two files have the same name and size, then an md5sum is computed to ensure that they are in fact exact duplicates.  In a future revision, you will be able to control the filter for your definition of "duplicate"

Intermediate Data Format
========================
Deckard's intermediate data format is a tab separated list of 4 values per line.  Each line represents a single file and each line has the following elements.

  filesize  - file size in bytes

  filename  - the base file name and extension of the file.  i.e. readme.rst

  md5sum    - either 32 spaces if it has not been calculated or the md5 hash

  filepath  - the file path and name with respect to the initial root directory given in the --seek phase.

