#!/usr/bin/env python
'''duplicate (by size/name/hash) detection and reporting utility'''
import sys

import hashlib
import os


def sorted_stdin():
    '''take the deckard research data from stdin, and sort it with knowledge
    that the first part is an integer and not a simple string. creating an
    iterator object'''

    def keyfn(val):
        '''specific sort functin for deckard research data'''
        fsize, fname, cksum, fpath = val.strip().split('\t')
        return (int(fsize), fname, cksum, fpath)

    for grp in sorted(sys.stdin.readlines(), key=keyfn):
        yield grp.strip()


def seek_replicants(rootdir):
    '''create a listing of all files, under the rootdir'''
    for root, _subfolders, files in os.walk(rootdir):
        for fname in files:
            fpath = os.path.join(root, fname)
            if not os.path.islink(fpath):
                stats = os.stat(fpath)
                print '\t'.join([str(stats.st_size), fname, ' ' * 32, fpath])


def test_replicants():
    '''test the replicant candidates -- need to look ahead 1'''
    last_fsize = -1  # one previous
    last_cksum = ' ' * 32
    last_fname = ''
    last_fpath = ''
    for line in sorted_stdin():
        fsize, fname, cksum, fpath = line.strip().split('\t')
        if last_fsize == -1:
            last_fsize = fsize
            last_cksum = cksum
            last_fname = fname
            last_fpath = fpath
            continue    # next iteration of loop
        if last_fsize == fsize and last_fname == fname:
            if not last_cksum.strip():
                last_cksum = hashlib.md5(file(last_fpath, 'rb').read()
                                         ).hexdigest()
            cksum = hashlib.md5(file(fpath, 'rb').read()).hexdigest()

        print '\t'.join([last_fsize, last_fname, last_cksum, last_fpath])
        last_fsize = fsize
        last_cksum = cksum
        last_fname = fname
        last_fpath = fpath

    print '\t'.join([last_fsize, last_fname, last_cksum, last_fpath])


def report_replicants():
    '''using the ouput of test_replicants, report statistics on duplicity'''
    last_fsize = -1  # one previous
    last_cksum = ' ' * 32
    last_fname = ''
    last_fpath = ''
    duplicate_count = 0
    duplicate_size = 0
    total_count = 0
    total_size = 0
    for line in sorted_stdin():
        fsize, fname, cksum, fpath = line.strip().split('\t')
        if last_fsize == -1:
            last_fsize = fsize
            last_cksum = cksum
            last_fname = fname
            last_fpath = fpath
            total_count += 1
            total_size += int(fsize)
            continue    # next iteration of loop

        if last_fsize == fsize and last_fname == fname and last_cksum == cksum:
            duplicate_count += 1
            duplicate_size += int(fsize)

        total_count += 1
        total_size += int(fsize)
        #print '\t'.join([last_fsize, last_fname, last_cksum, last_fpath])

        last_fsize = fsize
        last_cksum = cksum
        last_fname = fname
        last_fpath = fpath

    print "tc:%i dc:%i %f%%" % (
        total_count, duplicate_count, (duplicate_count * 100.0) / total_count)
    print "ts:%i ds:%i %f%%" % (
        total_size, duplicate_size, (duplicate_size * 100.0) / total_size)

if __name__ == '__main__':
    if '--seek' in sys.argv:
        ROOT_DIR = sys.argv[2]
        seek_replicants(ROOT_DIR)

    elif '--test' in sys.argv:
        test_replicants()

    elif '--report' in sys.argv:
        report_replicants()

    else:
        print 'huh?'
