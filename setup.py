from distutils.core import setup

setup(
    name='deckard',
    version='0.1.0',
    author='Jeff Hinrichs',
    author_email='jeffh@dundeemt.com',
    packages=[],
    scripts=['bin/deckard.py',],
    url='http://pypi.python.org/pypi/deckard/',
    license='LICENSE.txt',
    description='utility for finding, verifying and reporting on duplicate files',
    long_description=open('README.txt').read(),
    install_requires=[],
)